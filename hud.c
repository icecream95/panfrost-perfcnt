#include <errno.h>
#include <fcntl.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>

#include <libdrm/drm.h>

/* Not all distributions have panfrost_drm.h yet, so we need to do
 * some kill-yank. The following section is licensed:
 */

/* SPDX-License-Identifier: MIT */
/*
 * Copyright © 2014-2018 Broadcom
 * Copyright © 2019 Collabora ltd.
 */

#define DRM_PANFROST_GET_PARAM                  0x04
#define DRM_PANFROST_PERFCNT_ENABLE             0x06
#define DRM_PANFROST_PERFCNT_DUMP               0x07

#define DRM_IOCTL_PANFROST_GET_PARAM            DRM_IOWR(DRM_COMMAND_BASE + DRM_PANFROST_GET_PARAM, struct drm_panfrost_get_param)
#define DRM_IOCTL_PANFROST_PERFCNT_ENABLE       DRM_IOW(DRM_COMMAND_BASE + DRM_PANFROST_PERFCNT_ENABLE, struct drm_panfrost_perfcnt_enable)
#define DRM_IOCTL_PANFROST_PERFCNT_DUMP         DRM_IOW(DRM_COMMAND_BASE + DRM_PANFROST_PERFCNT_DUMP, struct drm_panfrost_perfcnt_dump)

enum drm_panfrost_param {
        DRM_PANFROST_PARAM_GPU_PROD_ID,
        DRM_PANFROST_PARAM_GPU_REVISION,
        DRM_PANFROST_PARAM_SHADER_PRESENT,
};

struct drm_panfrost_get_param {
        __u32 param;
        __u32 pad;
        __u64 value;
};

struct drm_panfrost_perfcnt_enable {
        __u32 enable;
        /*
         * On bifrost we have 2 sets of counters, this parameter defines the
         * one to track.
         */
        __u32 counterset;
};

struct drm_panfrost_perfcnt_dump {
        __u64 buf_ptr;
};



#define DEVICE "/dev/dri/renderD128"

void usage(const char* name) {
    printf("USAGE: %s command [args]\n\n", name);
    printf("Runs command, showing counter data through Gallium HUD\n");
    printf("Example:\nGALLIUM_HUD=pan-hz-GPU_ACTIVE,pan-frame-TI_TRIANGLES"
           ",pan-FRAG_PRIMITIVES+pan-FRAG_PRIMITIVES_DROPPED"
           " %s glxgears\n", name);
}

void wait() {
    struct timespec sleep = {
        // 0.5 seconds
        .tv_sec = 0,
        .tv_nsec = 500000000,
    };

    nanosleep(&sleep, NULL);
}

int main(int argc, char* argv[])
{
    if (argc == 1) {
        usage(argv[0]);
        return 1;
    }

    int fd = open(DEVICE, O_RDWR);

    {
        char name[9] = {0};

        drm_version_t ver = {0};

        ver.name = name;
        ver.name_len = 8;

        ioctl(fd, DRM_IOCTL_VERSION, &ver);

        if (strcmp(name, "panfrost")) {
            printf("%s is not using the Panfrost driver!\n", DEVICE);
            return 1;
        }
    }

    const char* map_file = NULL;

    {
        struct drm_panfrost_get_param p = {0};

        p.param = DRM_PANFROST_PARAM_GPU_PROD_ID;
        ioctl(fd, DRM_IOCTL_PANFROST_GET_PARAM, &p);

        switch (p.value) {
#define CASE(x) case x: map_file = "mapping-" #x ".txt"; break
            CASE(0x720);
            CASE(0x750);
            CASE(0x820);
            CASE(0x860);
#undef CASE
        default:
            printf("Unsupported GPU %llx\n", p.value);
            return 1;
        }
    }

    int shader_cores = 0;

    {
        struct drm_panfrost_get_param p = {0};

        p.param = DRM_PANFROST_PARAM_SHADER_PRESENT;
        ioctl(fd, DRM_IOCTL_PANFROST_GET_PARAM, &p);

        uint32_t c = p.value;

        do
            shader_cores += c & 1;
        while (c >>= 1);
    }

    close(fd);

    /*
     * One counter block for each of job manager, tiler, and L2 / MMU,
     * then the rest are for shader cores.
     *
     * It seems the blocks are in a different order to kbase, with all
     * of the shader blocks at the end.
     */
    int SIZE = 64 * (3 + shader_cores);

    uint32_t *buffer = calloc(SIZE, 4);

    char *names[256];
    int cnt_len = 0;

    int fildes[256][2];

    for (int i = 0; i < cnt_len; ++i)
        pipe(fildes[i]);

    char *hud = getenv("GALLIUM_HUD");

    if (!hud || !*hud) {
        usage(argv[0]);
        return 1;
    }

    char new_hud[4096] = {0};

    for (;;) {
        char *pan = strstr(hud, "pan-");

        if (!pan) {
            strcat(new_hud, hud);
            break;
        }

        strncat(new_hud, hud, pan - hud);

        pan += 4;

        const char* mode = NULL;

        if (!strncmp(pan, "hz-", 3)) {
            pan += 3;
            mode = "hz";
        } else if (!strncmp(pan, "frame-", 6)) {
            pan += 6;
            mode = "per-frame";
        } else {
            mode = "per-sec";
        }

        int renamed = 0;

        hud = pan;

        while (*hud) {
            switch (*hud) {
            case '=':
                renamed = 1;
                break;
            case '+': case ':': case ',': case ';':
                break;
            default:
                ++hud;
                continue;
            }
            break;
        }

        int len = hud - pan;
        names[cnt_len] = malloc(len + 1);
        strncpy(names[cnt_len], pan, len);
        names[cnt_len][len] = '\0';

        pipe(fildes[cnt_len]);

        char *nh_next = new_hud + strlen(new_hud);

        if (renamed)
            sprintf(nh_next, "fd-%s-%i", mode, fildes[cnt_len][0]);
        else
            sprintf(nh_next, "fd-%s-%i=%s", mode, fildes[cnt_len][0],
                    names[cnt_len]);

        ++cnt_len;

        if (cnt_len > 250) {
            printf("Too many counters listed\n");
            return 1;
        }
    }

    setenv("GALLIUM_HUD", new_hud, 1);

    int *num = calloc(cnt_len, sizeof(int));

    {
        FILE *mf = fopen(map_file, "r");

        for (;;) {
            unsigned rec_num = 0;
            char rec_name[80];

            int ret = fscanf(mf, "%x %78s", &rec_num, rec_name);

            if (ret != 2)
                break;

            if (rec_num >= 0xc0)
                rec_num -= 0x40;
            else if (rec_num >= 0x80)
                rec_num += 0x40;

            for (int i = 0; i < cnt_len; ++i) {
                if (!strcmp(names[i], rec_name)) {
                    num[i] = rec_num;
                }
            }
        }

        fclose(mf);
    }

    for (int i = 0; i < cnt_len; ++i) {
        if (!num[i]) {
            printf("Unrecognised counter %s\n", names[i]);
            return 1;
        }
    }

    int pid = fork();

    if (pid == -1)
        return 1;

    if (!pid) { /* child */
        for (int i = 0; i < cnt_len; ++i)
            close(fildes[i][1]);

        execvp(argv[1], argv + 1);

        return 0;
    }

    for (int i = 0; i < cnt_len; ++i)
        close(fildes[i][0]);

    fd = open(DEVICE, O_RDWR);

    struct drm_panfrost_perfcnt_enable en;
    en.counterset = 0;

    for (;;) {
        errno = 0;

        en.enable = 1;
        ioctl(fd, DRM_IOCTL_PANFROST_PERFCNT_ENABLE, &en);

        if (errno == ENOSYS) {
            printf("Performance counters are disabled.\n");
            printf("Enable them with:\n    "
                   "echo Y | sudo tee /sys/module/panfrost/parameters/unstable_ioctls\n");
            return 1;
        }
        else if (errno) {
            printf("Could not enable performance counters.\n");
            return 1;
        }

        wait();

        struct drm_panfrost_perfcnt_dump d;
        d.buf_ptr = (size_t)buffer;

        ioctl(fd, DRM_IOCTL_PANFROST_PERFCNT_DUMP, &d);

        en.enable = 0;
        ioctl(fd, DRM_IOCTL_PANFROST_PERFCNT_ENABLE, &en);

        uint32_t* bufptr = (uint32_t*)(size_t)d.buf_ptr;

        for (int i = 0; i < cnt_len; ++i) {
            // Multiply by 2 as we are only sleeping half a second
            long long val = bufptr[num[i]] * 2;

            if (num[i] >= 0xc0) {
                // Sum the numbers from all the shader cores
                for (int j = 1; j < shader_cores; ++j)
                    val += bufptr[num[i] + j*0x40] * 2;
            }

            char buf[20];

            int count = snprintf(buf, 20, "%lli\n", val);

            write(fildes[i][1], buf, count);
        }
    }
}
