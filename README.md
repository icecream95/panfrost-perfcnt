# Panfrost performance counter utilities

## Getting started:

Compile the program to capture performance counter data:

```
$ make
```

Enable performance counters:

```
$ echo Y | sudo tee /sys/module/panfrost/parameters/unstable_ioctls
```

Capture data from perf counters:

```
$ glmark2-es2 -b refract --off-screen &
$ ./perf 5 | tee perf.txt
```

Label the data:

```
$ ./parse.py perf.txt | less
```

You can also try a second program to print selected performace counters:

```
$ ./stream GPU_ACTIVE TRIPIPE_ACTIVE
```

With the patches from
[this](https://gitlab.freedesktop.org/icecream95/mesa/-/commits/hud-fd)
Mesa branch, it's also possible to use performance counters with
Gallium HUD:

```
$ GALLIUM_HUD=pan-hz-GPU_ACTIVE,pan-frame-TI_TRIANGLES,pan-FRAG_PRIMITIVES ./hud glxgears
```

Performance counters are global, and are summed for the duration of
the program runtime, which is controlled by the command-line
argument. Be aware that counters can saturate when capturing for more
than 5 seconds or so.

A description of most of the performance counters can be found at:

https://community.arm.com/developer/tools-software/graphics/b/blog/posts/mali-midgard-family-performance-counters

https://community.arm.com/developer/tools-software/graphics/b/blog/posts/mali-bifrost-family-performance-counters

`mapping-*.txt`, which are used to label counters, are based on
tables from the ARM Mali kernel driver. The script `gen_mapping.sh` can
be used to regenerate this file.
