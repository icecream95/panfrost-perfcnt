#include <errno.h>
#include <fcntl.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <time.h>
#include <unistd.h>

#include <libdrm/drm.h>

/* Not all distributions have panfrost_drm.h yet, so we need to do
 * some kill-yank. The following section is licensed:
 */

/* SPDX-License-Identifier: MIT */
/*
 * Copyright © 2014-2018 Broadcom
 * Copyright © 2019 Collabora ltd.
 */

#define DRM_PANFROST_GET_PARAM                  0x04
#define DRM_PANFROST_PERFCNT_ENABLE             0x06
#define DRM_PANFROST_PERFCNT_DUMP               0x07

#define DRM_IOCTL_PANFROST_GET_PARAM            DRM_IOWR(DRM_COMMAND_BASE + DRM_PANFROST_GET_PARAM, struct drm_panfrost_get_param)
#define DRM_IOCTL_PANFROST_PERFCNT_ENABLE       DRM_IOW(DRM_COMMAND_BASE + DRM_PANFROST_PERFCNT_ENABLE, struct drm_panfrost_perfcnt_enable)
#define DRM_IOCTL_PANFROST_PERFCNT_DUMP         DRM_IOW(DRM_COMMAND_BASE + DRM_PANFROST_PERFCNT_DUMP, struct drm_panfrost_perfcnt_dump)

enum drm_panfrost_param {
        DRM_PANFROST_PARAM_GPU_PROD_ID,
        DRM_PANFROST_PARAM_GPU_REVISION,
        DRM_PANFROST_PARAM_SHADER_PRESENT,
};

struct drm_panfrost_get_param {
        __u32 param;
        __u32 pad;
        __u64 value;
};

struct drm_panfrost_perfcnt_enable {
        __u32 enable;
        /*
         * On bifrost we have 2 sets of counters, this parameter defines the
         * one to track.
         */
        __u32 counterset;
};

struct drm_panfrost_perfcnt_dump {
        __u64 buf_ptr;
};



#define DEVICE "/dev/dri/renderD128"

void usage(const char* name) {
    printf("USAGE: %s COUNTERS...\n\n", name);
    printf("Prints Panfrost perf counter data for each of the counters\n");
    printf("from %s every 0.5 seconds.\n", DEVICE);
}

int main(int argc, char* argv[])
{
    struct timespec sleep = {
        .tv_sec = 0,
        .tv_nsec = 0
    };

    if (argc == 1) {
        usage(argv[0]);
        return 1;
    }

    {
        double sleep_time = 0.5;

        sleep.tv_sec = sleep_time;
        sleep.tv_nsec = (sleep_time - sleep.tv_sec) * 1000000000;
    }

    int fd = open(DEVICE, O_RDWR);

    {
        char name[16] = {0};

        drm_version_t ver = {0};

        ver.name = name;
        ver.name_len = 15;

        ioctl(fd, DRM_IOCTL_VERSION, &ver);

        if (strcmp(name, "panfrost")) {
            printf("%s is not using the Panfrost driver!\n", DEVICE);
            return 1;
        }
    }

    const char* map_file = NULL;

    {
        struct drm_panfrost_get_param p = {0};

        p.param = DRM_PANFROST_PARAM_GPU_PROD_ID;
        ioctl(fd, DRM_IOCTL_PANFROST_GET_PARAM, &p);

        switch (p.value) {
#define CASE(x) case x: map_file = "mapping-" #x ".txt"; break
            CASE(0x720);
            CASE(0x750);
            CASE(0x820);
            CASE(0x860);
        default:
            printf("Unsupported GPU %llx\n", p.value);
            return 1;
        }
    }

    int shader_cores = 0;

    {
        struct drm_panfrost_get_param p = {0};

        p.param = DRM_PANFROST_PARAM_SHADER_PRESENT;
        ioctl(fd, DRM_IOCTL_PANFROST_GET_PARAM, &p);

        uint32_t c = p.value;

        do
            shader_cores += c & 1;
        while (c >>= 1);
    }

    /*
     * One counter block for each of job manager, tiler, and L2 / MMU,
     * then the rest are for shader cores.
     *
     * It seems the blocks are in a different order to kbase, with all
     * of the shader blocks at the end.
     */
    int SIZE = 64 * (3 + shader_cores);

    uint32_t *buffer = calloc(SIZE, 4);

    char **names = argv + 1;
    int num[1024];
    memset(num, 0, sizeof(num));
    int cnt_len = argc - 1;

    if (cnt_len > 1023) {
        printf("Too many counters listed\n");
        return 1;
    }

    {
        FILE *mf = fopen(map_file, "r");

        for (;;) {
            unsigned rec_num = 0;
            char rec_name[80];

            int ret = fscanf(mf, "%x %78s", &rec_num, rec_name);

            if (ret != 2)
                break;

            if (rec_num >= 0xc0)
                rec_num -= 0x40;
            else if (rec_num >= 0x80)
                rec_num += 0x40;

            for (int i = 0; i < cnt_len; ++i) {
                if (!strcmp(names[i], rec_name)) {
                    num[i] = rec_num;
                }
            }
        }

        fclose(mf);
    }

    {
        int exit = 0;
        for (int i = 0; i < cnt_len; ++i) {
            if (!num[i]) {
                printf("Could not find performance counter %s in %s\n",
                       names[i], map_file);
                exit = 1;
            }
        }
        if (exit)
            return 1;
    }

    struct drm_panfrost_perfcnt_enable en;
    en.counterset = 0;

    for (;;) {
        en.enable = 1;
        ioctl(fd, DRM_IOCTL_PANFROST_PERFCNT_ENABLE, &en);

        if (errno == ENOSYS) {
            printf("Performance counters are disabled.\n");
            printf("Enable them with:\n    "
                   "echo Y | sudo tee /sys/module/panfrost/parameters/unstable_ioctls\n");
            return 1;
        }
        else if (errno) {
            printf("Could not enable performance counters.\n");
            return 1;
        }

        nanosleep(&sleep, NULL);

        struct drm_panfrost_perfcnt_dump d;
        d.buf_ptr = (size_t)buffer;

        ioctl(fd, DRM_IOCTL_PANFROST_PERFCNT_DUMP, &d);

        en.enable = 0;
        ioctl(fd, DRM_IOCTL_PANFROST_PERFCNT_ENABLE, &en);

        uint32_t* bufptr = (uint32_t*)(size_t)d.buf_ptr;

        for (int i = 0; i < cnt_len; ++i) {
            // Multiply by 2 as we are only sleeping half a second
            long long val = bufptr[num[i]] * 2;

            if (num[i] >= 0xc0) {
                // Sum the numbers from all the shader cores
                for (int j = 1; j < shader_cores; ++j)
                    val += bufptr[num[i + j*0x40]] * 2;
            }

            printf("%s\t%lli\n", names[i], val);
        }
    }
}
