#!/usr/bin/env python3

import os
import sys

def get_mapping(fname):
    mapping = {}

    with open(os.path.join(os.path.dirname(os.path.realpath(__file__)),
                           fname)) as m:
        for line in m:
            num, name = line.strip().split(" ", maxsplit=1)

            # Convert from hex
            num = int(num, 16)

            if num >= 0xc0:
                num -= 0x40
            elif num >= 0x80:
                num += 0x40

                for i in range(1, 8):
                    mapping[num + 0x40 * i] = name

            mapping[num] = name

    return mapping

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print(f"USAGE: {sys.argv[0]} PERF_FILE")
        exit(1)

    mapping = None

    file_ = sys.argv[1]

    if file_ == "-":
        file_ = "/dev/stdin"

    with open(file_) as f:
        for line in f:
            if not mapping:
                if line.split(" ")[0] != "GPU":
                    print("Expected a GPU ID line")
                    exit(1)

                gpu_id = line.strip().split(" ")[1]

                mapping = get_mapping(f"mapping-{gpu_id}.txt")

                print(line)
                continue

            line = line.rstrip("\n")

            if line.startswith("Dumped"):
                print(line)
                continue

            num = int(line.split(" ")[0].strip(":"), 16)

            if num in mapping:
                padding = " " * (29 - len(line))
                print(line + padding + mapping[num])
            else:
                print(line)
