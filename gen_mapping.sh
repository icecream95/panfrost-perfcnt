#!/usr/bin/env bash

if [ "$1" ]; then
    if [ ! -e "$1" ]; then
        printf "USAGE: %s [path to gator_events_mali_midgard_hw.c]\n" "$0"
        printf "\nThe source file will be downloaded with curl if needed.\n\n"
        exit 1
    fi

    SRC="$1"
    DEL=0
else
    SRC="$(mktemp)"
    echo "$SRC"
    DEL=1

    curl -L https://developer.arm.com/-/media/Files/downloads/mali-drivers/kernel/mali-midgard-gpu/TX011-SW-99002-r19p0-01rel0.tgz |
        tar xzO TX011-SW-99002-r19p0-01rel0/driver/product/kernel/drivers/gpu/arm/midgard/mali_kbase_gator_hwcnt_names.h \
                TX011-SW-99002-r19p0-01rel0/driver/product/kernel/drivers/gpu/arm/midgard/mali_kbase_gator_hwcnt_names_thex.h \
            >"$SRC"
fi

for x in t72x.0x720 t76x.0x750 t82x.0x820 t86x.0x860 tHEx.0x6221
do
    GPU=$(echo $x | cut -d. -f1)
    OUT=mapping-$(echo $x | cut -d. -f2).txt

    cat "$SRC" |
        sed -n '/hardware_counters_mali_'$GPU'\[\]/,/}/p' |
        sed 's@/\*.*\*/@@g' |
        grep '".*"' |
        tr -d '\t ",' |
        cut -d_ -f2- |
        sed 's/^$/-/' |
        awk '{ printf("%03x %s\n", NR - 1, $0); }' \
            >$OUT
done
