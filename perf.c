/* For asprintf */
#define _GNU_SOURCE

#include <errno.h>
#include <fcntl.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <time.h>
#include <unistd.h>

#include <libdrm/drm.h>

/* Not all distributions have panfrost_drm.h yet, so we need to do
 * some kill-yank. The following section is licensed:
 */

/* SPDX-License-Identifier: MIT */
/*
 * Copyright © 2014-2018 Broadcom
 * Copyright © 2019 Collabora ltd.
 */

#define DRM_PANFROST_GET_PARAM                  0x04
#define DRM_PANFROST_PERFCNT_ENABLE             0x06
#define DRM_PANFROST_PERFCNT_DUMP               0x07

#define DRM_IOCTL_PANFROST_GET_PARAM            DRM_IOWR(DRM_COMMAND_BASE + DRM_PANFROST_GET_PARAM, struct drm_panfrost_get_param)
#define DRM_IOCTL_PANFROST_PERFCNT_ENABLE       DRM_IOW(DRM_COMMAND_BASE + DRM_PANFROST_PERFCNT_ENABLE, struct drm_panfrost_perfcnt_enable)
#define DRM_IOCTL_PANFROST_PERFCNT_DUMP         DRM_IOW(DRM_COMMAND_BASE + DRM_PANFROST_PERFCNT_DUMP, struct drm_panfrost_perfcnt_dump)

enum drm_panfrost_param {
        DRM_PANFROST_PARAM_GPU_PROD_ID,
};

struct drm_panfrost_get_param {
        __u32 param;
        __u32 pad;
        __u64 value;
};

struct drm_panfrost_perfcnt_enable {
        __u32 enable;
        /*
         * On bifrost we have 2 sets of counters, this parameter defines the
         * one to track.
         */
        __u32 counterset;
};

struct drm_panfrost_perfcnt_dump {
        __u64 buf_ptr;
};



void usage(const char* name) {
    printf("USAGE: %s [TIME]\n\n", name);
    printf("Collects Panfrost perf counter data for TIME seconds.\n");
    printf("TIME defaults to 1 second.\n");
}

int main(int argc, char* argv[])
{
    struct timespec sleep = {
        .tv_sec = 1,
        .tv_nsec = 0
    };

    if (argc > 2) {
        usage(argv[0]);
        return 1;
    }

    if (argc == 2) {
        double sleep_time = strtod(argv[1], NULL);
        if (sleep_time == 0) {
            usage(argv[0]);
            return 1;
        }

        sleep.tv_sec = sleep_time;
        sleep.tv_nsec = (sleep_time - sleep.tv_sec) * 1000000000;
    }

#define SIZE 64 * 12

    uint32_t buffer[SIZE];
    memset(buffer, 0, sizeof(buffer));

    int fd = -1;

    for (int i = 128; i < 256; ++i) {
        char *device;
        asprintf(&device, "/dev/dri/renderD%i", i);

        fd = open(device, O_RDWR);
        free(device);

        if (fd == -1)
            continue;

        char name[16] = {0};

        drm_version_t ver = {0};

        ver.name = name;
        ver.name_len = 15;

        ioctl(fd, DRM_IOCTL_VERSION, &ver);

        if (!strcmp(name, "panfrost"))
            break;
    }

    {
        struct drm_panfrost_get_param p = {0};

        p.param = DRM_PANFROST_PARAM_GPU_PROD_ID;
        ioctl(fd, DRM_IOCTL_PANFROST_GET_PARAM, &p);
        printf("GPU 0x%llx\n", p.value);
    }

    struct drm_panfrost_perfcnt_enable en;
    en.counterset = 0;

    en.enable = 1;
    ioctl(fd, DRM_IOCTL_PANFROST_PERFCNT_ENABLE, &en);

    if (errno == ENOSYS) {
        printf("Performance counters are disabled.\n");
        printf("Enable them with:\n    "
            "echo Y | sudo tee /sys/module/panfrost/parameters/unstable_ioctls\n");
        return 1;
    }
    else if (errno) {
        printf("Could not enable performance counters.\n");
        return 1;
    }

    nanosleep(&sleep, NULL);

    struct drm_panfrost_perfcnt_dump d;
    d.buf_ptr = (size_t)buffer;

    ioctl(fd, DRM_IOCTL_PANFROST_PERFCNT_DUMP, &d);

    en.enable = 0;
    ioctl(fd, DRM_IOCTL_PANFROST_PERFCNT_ENABLE, &en);

    uint32_t* bufptr = (uint32_t*)(size_t)d.buf_ptr;

    for (unsigned i = 0; i < SIZE; ++i) {
        if (!(i & 0x3f) && !bufptr[i + 2]) {
            printf("Dumped %i values\n", i);
            break;
        }
        printf("%03x: %08x %u\n", i, bufptr[i], bufptr[i]);
    }
}
